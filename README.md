# Dockerfile
*This is my personal docker file for some purposes.*


## Bulid Image
```bash
$ docker build -t myimage .
```

## Create Container
```bash
$ docker create --name myhost -h myhost -p 222:22 myimage
$ docker start myhost
```

## Login
```bash
$ ssh -p 222 admin@127.0.0.1

# default password is admin99admin
# you should change the admin's password

$ passwd
```

